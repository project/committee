<?php

/**
 * @file
 * Contains committee.page.inc.
 *
 * Page callback for Committee entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Committee templates.
 *
 * Default template: committee.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_committee(array &$variables) {
  // Fetch Committee Entity Object.
  $committee = $variables['elements']['#committee'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
