<?php

/**
 * @file
 * Contains committee_meeting.page.inc.
 *
 * Page callback for Committee meeting entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Committee meeting templates.
 *
 * Default template: committee_meeting.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_committee_meeting(array &$variables) {
  // Fetch CommitteeMeeting Entity Object.
  $committee_meeting = $variables['elements']['#committee_meeting'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
