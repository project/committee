<?php

namespace Drupal\committee_meeting\Entity;

use Drupal\child_entity\ChildEntityViewsData;

/**
 * Provides Views data for Committee meeting entities.
 */
class CommitteeMeetingViewsData extends ChildEntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
