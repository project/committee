<?php

namespace Drupal\committee_meeting\Entity;

use Drupal\child_entity\ChildEntityTrait;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the Committee meeting entity.
 *
 * @ingroup committee_meeting
 *
 * @ContentEntityType(
 *   id = "committee_meeting",
 *   label = @Translation("Committee meeting"),
 *   label_collection = @Translation("Committee meetings"),
 *   label_singular = @Translation("Committee meeting"),
 *   label_plural = @Translation("Committee meetings"),
 *   label_count = @PluralTranslation(
 *     singular = "@count committee meeting",
 *     plural = "@count committee meetings",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\committee_meeting\Storage\CommitteeMeetingStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\committee_meeting\CommitteeMeetingListBuilder",
 *     "views_data" = "Drupal\committee_meeting\Entity\CommitteeMeetingViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\committee_meeting\Form\CommitteeMeetingForm",
 *       "add" = "Drupal\committee_meeting\Form\CommitteeMeetingForm",
 *       "edit" = "Drupal\committee_meeting\Form\CommitteeMeetingForm",
 *       "delete" = "Drupal\committee_meeting\Form\CommitteeMeetingDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\committee_meeting\Routing\CommitteeMeetingHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\committee_meeting\Access\CommitteeMeetingAccessControlHandler",
 *   },
 *   base_table = "committee_meeting",
 *   revision_table = "committee_meeting_revision",
 *   revision_data_table = "committee_meeting_field_revision",
 *   admin_permission = "administer committee meeting entities",
 *   collection_permission = "view committee meeting collection",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "owner" = "user_id",
 *     "parent" = "committee",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "canonical" = "/meeting/{committee_meeting}",
 *     "add-form" = "/meeting/add",
 *     "edit-form" = "/meeting/{committee_meeting}/edit",
 *     "delete-form" = "/meeting/{committee_meeting}/delete",
 *     "version-history" = "/meeting/{committee_meeting}/revisions",
 *     "revision" = "/meeting/{committee_meeting}/revisions/{committee_meeting_revision}/view",
 *     "revision_revert" = "/meeting/{committee_meeting}/revisions/{committee_meeting_revision}/revert",
 *     "revision_delete" = "/meeting/{committee_meeting}/revisions/{committee_meeting_revision}/delete",
 *     "collection" = "/meetings",
 *   },
 *   field_ui_base_route = "committee_meeting.settings"
 * )
 */
class CommitteeMeeting extends EditorialContentEntityBase implements CommitteeMeetingInterface {

  use EntityOwnerTrait;
  use ChildEntityTrait {
    urlRouteParameters as childEntityUrlRouteParameters;
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = $this->childEntityUrlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    $committee = $this->getParentEntity();
    $meeting_datetime = DrupalDateTime::createFromTimestamp($this->meeting_datetime->value);
    $formatted_date = \Drupal::service('date.formatter')->format($meeting_datetime->getTimestamp(), 'custom', 'j F Y \\a\\t h:ia');
    return $committee->label() . ' ' . t('meeting on') . ' ' . $formatted_date;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // If no revision author has been set explicitly,
    // make the committee_meeting owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::ownerBaseFieldDefinitions($entity_type);
    $fields += static::childBaseFieldDefinitions($entity_type);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['meeting_datetime'] = BaseFieldDefinition::create('smartdate')
      ->setLabel(t('Meeting Date'))
      ->setDescription(t('The start and optional end time of the meeting'))
      ->setRequired(TRUE)
      ->setDefaultValue([
        'default_date_type' => 'next_hour',
        'default_date' => '',
        'default_duration_increments' => "0 | No End Time\r\n30\r\n60|1 hour\r\n90\r\n120|2 hours\r\ncustom",
        'default_duration' => '60',
      ])
      ->setDisplayOptions('form', [
        'type' => 'smartdate_timezone',
        'weight' => -3,
        'settings' => [
          'modal' => TRUE,
          'default_tz' => 'user',
        ]
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['notes'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Notes'))
      ->setDescription(t('Notes for the Meeting'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => -1,
        'settings' => [
          'rows' => 5,
        ]
      ])
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'hidden',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
