<?php

namespace Drupal\committee_meeting\Entity;

use Drupal\child_entity\Entity\ChildEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Committee meeting entities.
 *
 * @ingroup committee_meeting
 */
interface CommitteeMeetingInterface
  extends ContentEntityInterface,
  RevisionLogInterface,
  EntityChangedInterface,
  EntityPublishedInterface,
  EntityOwnerInterface,
  ChildEntityInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Committee meeting creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Committee meeting.
   */
  public function getCreatedTime();

  /**
   * Sets the Committee meeting creation timestamp.
   *
   * @param int $timestamp
   *   The Committee meeting creation timestamp.
   *
   * @return \Drupal\committee_meeting\Entity\CommitteeMeetingInterface
   *   The called Committee meeting entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Committee meeting revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Committee meeting revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\committee_meeting\Entity\CommitteeMeetingInterface
   *   The called Committee meeting entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Committee meeting revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Committee meeting revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\committee_meeting\Entity\CommitteeMeetingInterface
   *   The called Committee meeting entity.
   */
  public function setRevisionUserId($uid);

}
