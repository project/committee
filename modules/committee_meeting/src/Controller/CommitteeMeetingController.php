<?php

namespace Drupal\committee_meeting\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\committee_meeting\Entity\CommitteeMeetingInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CommitteeMeetingController.
 *
 *  Returns responses for Committee meeting routes.
 */
class CommitteeMeetingController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Committee meeting revision.
   *
   * @param int $committee_meeting_revision
   *   The Committee meeting revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($committee_meeting_revision) {
    $committee_meeting = $this->entityTypeManager()->getStorage('committee_meeting')
      ->loadRevision($committee_meeting_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('committee_meeting');

    return $view_builder->view($committee_meeting);
  }

  /**
   * Page title callback for a Committee meeting revision.
   *
   * @param int $committee_meeting_revision
   *   The Committee meeting revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($committee_meeting_revision) {
    $committee_meeting = $this->entityTypeManager()->getStorage('committee_meeting')
      ->loadRevision($committee_meeting_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $committee_meeting->label(),
      '%date' => $this->dateFormatter->format($committee_meeting->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Committee meeting.
   *
   * @param \Drupal\committee_meeting\Entity\CommitteeMeetingInterface $committee_meeting
   *   A Committee meeting object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(CommitteeMeetingInterface $committee_meeting) {
    $account = $this->currentUser();
    $committee_meeting_storage = $this->entityTypeManager()->getStorage('committee_meeting');

    $build['#title'] = $this->t('Revisions for %title', ['%title' => $committee_meeting->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all committee meeting revisions") || $account->hasPermission('administer committee meeting entities')));
    $delete_permission = (($account->hasPermission("delete all committee meeting revisions") || $account->hasPermission('administer committee meeting entities')));

    $rows = [];

    $vids = $committee_meeting_storage->revisionIds($committee_meeting);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\committee_meeting\CommitteeMeetingInterface $revision */
      $revision = $committee_meeting_storage->loadRevision($vid);
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $committee_meeting->getRevisionId()) {
          $link = $this->l($date, new Url('entity.committee_meeting.revision', [
            'committee_meeting' => $committee_meeting->id(),
            'committee_meeting_revision' => $vid,
          ]));
        }
        else {
          $link = $committee_meeting->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => Url::fromRoute('entity.committee_meeting.revision_revert', [
                'committee_meeting' => $committee_meeting->id(),
                'committee_meeting_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.committee_meeting.revision_delete', [
                'committee_meeting' => $committee_meeting->id(),
                'committee_meeting_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
    }

    $build['committee_meeting_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
