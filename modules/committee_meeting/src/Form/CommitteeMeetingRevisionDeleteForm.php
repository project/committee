<?php

namespace Drupal\committee_meeting\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Committee meeting revision.
 *
 * @ingroup committee_meeting
 */
class CommitteeMeetingRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Committee meeting revision.
   *
   * @var \Drupal\committee_meeting\Entity\CommitteeMeetingInterface
   */
  protected $revision;

  /**
   * The Committee meeting storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $committeeMeetingStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->committeeMeetingStorage = $container->get('entity_type.manager')->getStorage('committee_meeting');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'committee_meeting_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.committee_meeting.version_history', ['committee_meeting' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $committee_meeting_revision = NULL) {
    $this->revision = $this->CommitteeMeetingStorage->loadRevision($committee_meeting_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->CommitteeMeetingStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Committee meeting: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage(t('Revision from %revision-date of Committee meeting %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.committee_meeting.canonical',
       ['committee_meeting' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {committee_meeting_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.committee_meeting.version_history',
         ['committee_meeting' => $this->revision->id()]
      );
    }
  }

}
