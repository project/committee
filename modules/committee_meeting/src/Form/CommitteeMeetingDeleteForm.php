<?php

namespace Drupal\committee_meeting\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Committee meeting entities.
 *
 * @ingroup committee_meeting
 */
class CommitteeMeetingDeleteForm extends ContentEntityDeleteForm {


}
