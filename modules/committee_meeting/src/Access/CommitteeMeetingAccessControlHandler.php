<?php

namespace Drupal\committee_meeting\Access;

use Drupal\child_entity\ChildEntityAccessControlHandler;
use Drupal\committee_meeting\Entity\CommitteeMeetingInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Committee meeting entity.
 *
 * @see \Drupal\committee_meeting\Entity\CommitteeMeeting.
 */
class CommitteeMeetingAccessControlHandler extends ChildEntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
//  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
//    /** @var CommitteeMeetingInterface $entity */
//
//    // @TODO Add checks for committee membership
//    return parent::checkAccess($entity, $operation, $account);
//  }

}
