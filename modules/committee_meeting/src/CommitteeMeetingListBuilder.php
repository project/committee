<?php

namespace Drupal\committee_meeting;

use Drupal\child_entity\ChildEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Committee meeting entities.
 *
 * @ingroup committee_meeting
 */
class CommitteeMeetingListBuilder extends ChildEntityListBuilder {


}
