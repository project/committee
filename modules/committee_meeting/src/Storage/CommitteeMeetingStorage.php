<?php

namespace Drupal\committee_meeting\Storage;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\committee_meeting\Entity\CommitteeMeetingInterface;

/**
 * Defines the storage handler class for Committee meeting entities.
 *
 * This extends the base storage class, adding required special handling for
 * Committee meeting entities.
 *
 * @ingroup committee_meeting
 */
class CommitteeMeetingStorage extends SqlContentEntityStorage implements CommitteeMeetingStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(CommitteeMeetingInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {committee_meeting_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {committee_meeting_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

}
