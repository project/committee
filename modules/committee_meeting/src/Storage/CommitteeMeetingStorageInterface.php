<?php

namespace Drupal\committee_meeting\Storage;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\committee_meeting\Entity\CommitteeMeetingInterface;

/**
 * Defines the storage handler class for Committee meeting entities.
 *
 * This extends the base storage class, adding required special handling for
 * Committee meeting entities.
 *
 * @ingroup committee_meeting
 */
interface CommitteeMeetingStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Committee meeting revision IDs for a specific Committee meeting.
   *
   * @param \Drupal\committee_meeting\Entity\CommitteeMeetingInterface $entity
   *   The Committee meeting entity.
   *
   * @return int[]
   *   Committee meeting revision IDs (in ascending order).
   */
  public function revisionIds(CommitteeMeetingInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Committee meeting author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Committee meeting revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

}
