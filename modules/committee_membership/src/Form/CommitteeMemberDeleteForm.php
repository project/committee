<?php

namespace Drupal\committee_membership\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Committee member entities.
 *
 * @ingroup committee_membership
 */
class CommitteeMemberDeleteForm extends ContentEntityDeleteForm {


}
