<?php

namespace Drupal\committee_membership\Form;

use Drupal\child_entity\Form\ChildContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Committee member edit forms.
 *
 * @ingroup committee_membership
 */
class CommitteeMemberForm extends ChildContentEntityForm {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var \Drupal\committee_membership\Entity\CommitteeMember $entity */
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Committee member.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Committee member.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.committee_member.canonical', [
      'committee' => $this->getParentEntity()->id(),
      'committee_member' => $entity->id(),
    ]);
  }

}
