<?php

namespace Drupal\committee_membership\Entity;

use Drupal\child_entity\Entity\ChildEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Committee member entities.
 *
 * @ingroup committee_membership
 */
interface CommitteeMemberInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, ChildEntityInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Committee member creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Committee member.
   */
  public function getCreatedTime();

  /**
   * Sets the Committee member creation timestamp.
   *
   * @param int $timestamp
   *   The Committee member creation timestamp.
   *
   * @return \Drupal\committee_membership\Entity\CommitteeMemberInterface
   *   The called Committee member entity.
   */
  public function setCreatedTime($timestamp);

}
