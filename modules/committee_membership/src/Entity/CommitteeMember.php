<?php

namespace Drupal\committee_membership\Entity;

use Drupal\child_entity\ChildEntityTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerTrait;
use Drupal\user\UserInterface;

/**
 * Defines the Committee member entity.
 *
 * @ingroup committee_membership
 *
 * @ContentEntityType(
 *   id = "committee_member",
 *   label = @Translation("Committee member"),
 *   label_collection = @Translation("Committee membership"),
 *   label_singular = @Translation("Committee member"),
 *   label_plural = @Translation("Committee members"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Committee member",
 *     plural = "@count Committee members",
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\committee_membership\CommitteeMemberListBuilder",
 *     "views_data" = "Drupal\committee_membership\Entity\CommitteeMemberViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\committee_membership\Form\CommitteeMemberForm",
 *       "add" = "Drupal\committee_membership\Form\CommitteeMemberForm",
 *       "edit" = "Drupal\committee_membership\Form\CommitteeMemberForm",
 *       "delete" = "Drupal\committee_membership\Form\CommitteeMemberDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\committee_membership\Routing\CommitteeMemberHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\committee_membership\Access\CommitteeMemberAccessControlHandler",
 *   },
 *   base_table = "committee_member",
 *   translatable = FALSE,
 *   admin_permission = "administer committee member entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "owner" = "user_id",
 *     "parent" = "committee",
 *   },
 *   links = {
 *     "canonical" = "/member/{committee_member}",
 *     "add-form" = "/member/add",
 *     "edit-form" = "/member/{committee_member}/edit",
 *     "delete-form" = "/member/{committee_member}/delete",
 *     "collection" = "/members",
 *   },
 *   field_ui_base_route = "committee_member.settings"
 * )
 */
class CommitteeMember extends ContentEntityBase implements CommitteeMemberInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  use ChildEntityTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::ownerBaseFieldDefinitions($entity_type);
    $fields += static::childBaseFieldDefinitions($entity_type);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['member'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel('Member')
      ->setDescription('The contact who is a member of the committee')
      ->setSetting('target_type', 'user')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['start_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel('Start Date')
      ->setDescription('')
      ->setSetting('datetime_type', 'date')
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['end_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel('End Date')
      ->setDescription('')
      ->setSetting('datetime_type', 'date')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
