<?php

namespace Drupal\committee_membership\Access;

use Drupal\committee_membership\Entity\CommitteeMemberInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Committee member entity.
 *
 * @see \Drupal\committee_membership\Entity\CommitteeMember.
 */
class CommitteeMemberAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var CommitteeMemberInterface $entity */

    switch ($operation) {

      case 'view':

        return AccessResult::allowedIfHasPermission($account, 'view published committee member entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit committee member entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete committee member entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add committee member entities');
  }


}
