<?php

/**
 * @file
 * Contains committee_member.page.inc.
 *
 * Page callback for Committee member entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Committee member templates.
 *
 * Default template: committee_member.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_committee_member(array &$variables) {
  // Fetch CommitteeMember Entity Object.
  $committee_member = $variables['elements']['#committee_member'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
