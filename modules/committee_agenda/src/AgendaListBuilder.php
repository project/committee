<?php

namespace Drupal\committee_agenda;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Agenda entities.
 *
 * @ingroup committee_agenda
 */
class AgendaListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Agenda ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\committee_agenda\Entity\Agenda $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.committee_agenda.edit_form',
      ['committee_agenda' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
