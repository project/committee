<?php

namespace Drupal\committee_agenda\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AgendaTypeForm.
 */
class AgendaTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $committee_agenda_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $committee_agenda_type->label(),
      '#description' => $this->t("Label for the Agenda type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $committee_agenda_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\committee_agenda\Entity\AgendaType::load',
      ],
      '#disabled' => !$committee_agenda_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $committee_agenda_type = $this->entity;
    $status = $committee_agenda_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Agenda type.', [
          '%label' => $committee_agenda_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Agenda type.', [
          '%label' => $committee_agenda_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($committee_agenda_type->toUrl('collection'));
  }

}
