<?php

namespace Drupal\committee_agenda\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\field_ui\FieldUI;
use Drupal\paragraphs\Form\ParagraphsTypeForm;

/**
 * Class AgendaItemTypeForm.
 */
class AgendaItemTypeForm extends ParagraphsTypeForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $committee_agenda_item_type = $this->entity;

    if ($behavior_plugin_definitions = $this->paragraphsBehaviorManager->getApplicableDefinitions($committee_agenda_item_type)) {
      foreach ($behavior_plugin_definitions as $id => $behavior_plugin_definition) {
        $behavior_plugin = $committee_agenda_item_type->getBehaviorPlugin($id);

        // If the behavior is enabled, initialize the configuration with the
        // enabled key and then let it process the form input.
        if ($form_state->getValue(['behavior_plugins', $id, 'enabled'])) {
          $behavior_plugin->setConfiguration(['enabled' => TRUE]);
          if (isset($form['behavior_plugins'][$id]['settings'])) {
            $subform_state = SubformState::createForSubform($form['behavior_plugins'][$id]['settings'], $form, $form_state);
            $behavior_plugin->submitConfigurationForm($form['behavior_plugins'][$id]['settings'], $subform_state);
          }
        }
        else {
          // The plugin is not enabled, reset to default configuration.
          $behavior_plugin->setConfiguration([]);
        }
      }
    }

    $status = $committee_agenda_item_type->save();
    $this->messenger->addMessage($this->t('Saved the %label Agenda item type.', array(
      '%label' => $committee_agenda_item_type->label(),
    )));
    if (($status == SAVED_NEW && $this->moduleHandler->moduleExists('field_ui'))
      && $route_info = FieldUI::getOverviewRouteInfo('committee_agenda_item_type', $committee_agenda_item_type->id())) {
      $form_state->setRedirectUrl($route_info);
    }
    else {
      $form_state->setRedirect('entity.committee_agenda_item_type.collection');
    }
  }
}
