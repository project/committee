<?php

namespace Drupal\committee_agenda\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Agenda revision.
 *
 * @ingroup committee_agenda
 */
class AgendaRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Agenda revision.
   *
   * @var \Drupal\committee_agenda\Entity\AgendaInterface
   */
  protected $revision;

  /**
   * The Agenda storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $agendaStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->agendaStorage = $container->get('entity_type.manager')->getStorage('committee_agenda');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'committee_agenda_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.committee_agenda.version_history', ['committee_agenda' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $committee_agenda_revision = NULL) {
    $this->revision = $this->AgendaStorage->loadRevision($committee_agenda_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->AgendaStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Agenda: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage(t('Revision from %revision-date of Agenda %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.committee_agenda.canonical',
       ['committee_agenda' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {committee_agenda_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.committee_agenda.version_history',
         ['committee_agenda' => $this->revision->id()]
      );
    }
  }

}
