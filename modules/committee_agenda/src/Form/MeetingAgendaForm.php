<?php

namespace Drupal\committee_agenda\Form;

use Drupal\committee\Entity\CommitteeInterface;
use Drupal\committee_agenda\Entity\AgendaInterface;
use Drupal\committee_meeting\Entity\CommitteeMeetingInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Committee Meeting Agendas form.
 */
class MeetingAgendaForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'committee_agenda_meeting_agenda';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, CommitteeInterface $committee = NULL, CommitteeMeetingInterface $committee_meeting = NULL) {

    /** @var AgendaInterface $agenda */
    $agenda = !$committee_meeting->get('agenda')->isEmpty() ?  $committee_meeting->agenda->entity : NULL;
    $form_state->set('committee', $committee);
    $form_state->set('committee_meeting', $committee_meeting);

    $form['agenda'] = [
      '#type' => 'inline_entity_form',
      '#entity_type' => 'committee_agenda',
      '#default_value' => $agenda,
    ];

    // Add a submit button. Give it a class for easy JavaScript targeting.
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var AgendaInterface $agenda */
    $agenda = $form['agenda']['#entity'];
    $agenda_items = $agenda->get('agenda_items');
    /** @var EntityReferenceItem $agenda_item */
    foreach ($agenda_items as $agenda_item) {
      \Drupal::logger('committee_agenda')->notice(print_r($agenda_item->getValue(), TRUE));
    }

    $agenda->save();

    /** @var CommitteeMeetingInterface $committee */
    $committee = $form_state->get('committee');
    /** @var CommitteeMeetingInterface $committee_meeting */
    $committee_meeting = $form_state->get('committee_meeting');

    if($committee_meeting->get('agenda')->isEmpty()) {
      $committee_meeting->get('agenda')->setValue($agenda->id());
      $committee_meeting->save();
    }

    $form_state->setRedirect('entity.committee_meeting.committee_agenda', [
      'committee' => $committee->id(),
      'committee_meeting' => $committee_meeting->id(),
    ]);
  }

}
