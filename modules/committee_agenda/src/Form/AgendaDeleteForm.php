<?php

namespace Drupal\committee_agenda\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Agenda entities.
 *
 * @ingroup committee_agenda
 */
class AgendaDeleteForm extends ContentEntityDeleteForm {


}
