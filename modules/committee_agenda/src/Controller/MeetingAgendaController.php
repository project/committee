<?php

namespace Drupal\committee_agenda\Controller;

use Drupal\committee\Entity\CommitteeInterface;
use Drupal\committee_agenda\Entity\AgendaInterface;
use Drupal\committee_meeting\Entity\CommitteeMeetingInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Committee Meeting Agendas routes.
 */
class MeetingAgendaController extends ControllerBase {

  /**
   * Builds the response.
   * @param CommitteeInterface|null $committee
   * @param CommitteeMeetingInterface|null $committee_meeting
   * @return mixed
   */
  public function build(CommitteeInterface $committee = NULL, CommitteeMeetingInterface $committee_meeting = NULL) {

    if (!$committee_meeting->get('agenda')->isEmpty()) {
      /** @var AgendaInterface $agenda */
      $agenda = $committee_meeting->agenda->entity;

      $view_builder = $this->entityTypeManager()->getViewBuilder($agenda->getEntityTypeId());

      return $view_builder->view($agenda);
    }
    else {
      return [
        '#markup' => "No Agenda Yet",
        '#cache' => [
          'keys' => ['entity_view', 'committee_meeting', $committee_meeting->id()],
          'contexts' => ['languages'],
          'tags' => ['committee_meeting:' . $committee_meeting->id()],
          'max-age' => Cache::PERMANENT,
        ],
      ];
    }

  }

  /**
   * Return the Page title.
   * @param CommitteeInterface|null $committee
   * @param CommitteeMeetingInterface|null $committee_meeting
   * @return mixed
   */
  public function getTitle(CommitteeInterface $committee = NULL, CommitteeMeetingInterface $committee_meeting = NULL) {

    return $committee_meeting->label() . " Agenda";

  }

}
