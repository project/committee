<?php

namespace Drupal\committee_agenda\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\committee_agenda\Entity\AgendaInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AgendaController.
 *
 *  Returns responses for Agenda routes.
 */
class AgendaController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Agenda revision.
   *
   * @param int $committee_agenda_revision
   *   The Agenda revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($committee_agenda_revision) {
    $committee_agenda = $this->entityTypeManager()->getStorage('committee_agenda')
      ->loadRevision($committee_agenda_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('committee_agenda');

    return $view_builder->view($committee_agenda);
  }

  /**
   * Page title callback for a Agenda revision.
   *
   * @param int $committee_agenda_revision
   *   The Agenda revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($committee_agenda_revision) {
    $committee_agenda = $this->entityTypeManager()->getStorage('committee_agenda')
      ->loadRevision($committee_agenda_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $committee_agenda->label(),
      '%date' => $this->dateFormatter->format($committee_agenda->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Agenda.
   *
   * @param \Drupal\committee_agenda\Entity\AgendaInterface $committee_agenda
   *   A Agenda object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(AgendaInterface $committee_agenda) {
    $account = $this->currentUser();
    $committee_agenda_storage = $this->entityTypeManager()->getStorage('committee_agenda');

    $langcode = $committee_agenda->language()->getId();
    $langname = $committee_agenda->language()->getName();
    $languages = $committee_agenda->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $committee_agenda->label()]) : $this->t('Revisions for %title', ['%title' => $committee_agenda->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all agenda revisions") || $account->hasPermission('administer agenda entities')));
    $delete_permission = (($account->hasPermission("delete all agenda revisions") || $account->hasPermission('administer agenda entities')));

    $rows = [];

    $vids = $committee_agenda_storage->revisionIds($committee_agenda);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\committee_agenda\AgendaInterface $revision */
      $revision = $committee_agenda_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $committee_agenda->getRevisionId()) {
          $link = $this->l($date, new Url('entity.committee_agenda.revision', [
            'committee_agenda' => $committee_agenda->id(),
            'committee_agenda_revision' => $vid,
          ]));
        }
        else {
          $link = $committee_agenda->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.committee_agenda.translation_revert', [
                'committee_agenda' => $committee_agenda->id(),
                'committee_agenda_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.committee_agenda.revision_revert', [
                'committee_agenda' => $committee_agenda->id(),
                'committee_agenda_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.committee_agenda.revision_delete', [
                'committee_agenda' => $committee_agenda->id(),
                'committee_agenda_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['committee_agenda_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
