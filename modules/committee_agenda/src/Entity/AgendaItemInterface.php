<?php

namespace Drupal\committee_agenda\Entity;

use Drupal\paragraphs\ParagraphInterface;

/**
 * Provides an interface for defining Committee agenda item entities.
 *
 * @ingroup committee
 */
interface AgendaItemInterface extends ParagraphInterface {

}
