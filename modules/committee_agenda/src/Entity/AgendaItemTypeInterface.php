<?php

namespace Drupal\committee_agenda\Entity;

use Drupal\paragraphs\ParagraphsTypeInterface;

/**
 * Provides an interface for defining Committee agenda item type entities.
 */
interface AgendaItemTypeInterface extends ParagraphsTypeInterface {

}
