<?php

namespace Drupal\committee_agenda\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BundleFieldDefinition;
use Drupal\Core\Field\FieldDefinition;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\user\EntityOwnerTrait;
use Drupal\user\UserInterface;

/**
 * Defines the agenda item entity.
 *
 * @ingroup committee
 *
 * @ContentEntityType(
 *   id = "committee_agenda_item",
 *   label = @Translation("Agenda item"),
 *   label_collection = @Translation("Agenda items"),
 *   label_singular = @Translation("Agenda item"),
 *   label_plural = @Translation("Agenda items"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Agenda Item",
 *     plural = "@count Agenda Items",
 *   ),
 *   bundle_label = @Translation("Agenda item type"),
 *   handlers = {
 *     "access" = "Drupal\committee_agenda\Access\AgendaItemAccessControlHandler",
 *     "storage_schema" = "Drupal\paragraphs\ParagraphStorageSchema",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm"
 *     },
 *   },
 *   base_table = "committee_agenda_item",
 *   data_table = "committee_agenda_item_field_data",
 *   revision_table = "committee_agenda_item_revision",
 *   revision_data_table = "committee_agenda_item_revision_field_data",
 *   translatable = TRUE,
 *   entity_revision_parent_type_field = "parent_type",
 *   entity_revision_parent_id_field = "parent_id",
 *   entity_revision_parent_field_name_field = "parent_field_name",
 *   admin_permission = "administer committee agenda item entities",
 *   entity_keys = {
 *     "bundle" = "type",
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *     "revision" = "revision_id",
 *     "uuid" = "uuid",
 *   },
 *   bundle_entity_type = "committee_agenda_item_type",
 *   field_ui_base_route = "entity.committee_agenda_item_type.edit_form",
 *   common_reference_revisions_target = TRUE,
 *   content_translation_ui_skip = TRUE,
 *   render_cache = FALSE,
 *   default_reference_revision_settings = {
 *     "field_storage_config" = {
 *       "cardinality" = -1,
 *       "settings" = {
 *         "target_type" = "committee_agenda_item"
 *       }
 *     },
 *     "field_config" = {
 *       "settings" = {
 *         "handler" = "default:committee_agenda_item"
 *       }
 *     },
 *     "entity_form_display" = {
 *       "type" = "entity_reference_paragraphs"
 *     },
 *     "entity_view_display" = {
 *       "type" = "entity_reference_revisions_entity_view"
 *     }
 *   },
 *   serialized_field_property_names = {
 *     "behavior_settings" = {
 *       "value"
 *     }
 *   }
 * )
 */
class AgendaItem extends Paragraph implements AgendaItemInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->title->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Agenda Item Title'))
      ->setDescription(t('The name of the Committee agenda item entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['agenda_text'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Agenda description'))
      ->setDescription(t('A description of the agenda item'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => -2,
        'settings' => [
          'rows' => 5,
        ]
      ])
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'hidden',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['camera'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('In camera'))
      ->setDescription(t('If this agenda item is to be held in camera, all sub-items will also be in camera'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['deferred_agenda_item_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Deferred from'))
      ->setDescription(t('The original agenda item that this item was deferred from.'))
      ->setSetting('target_type', 'committee_agenda_item')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_label',
        'label' => 'inline',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   *
   * The Paragraph getSummaryItems doesn't use base fields. So here we override
   * to use out fields in AgendaItems
   */
  public function getSummaryItems(array $options = []) {
    $summary = ['content' => [], 'behaviors' => []];
    $show_behavior_summary = isset($options['show_behavior_summary']) ? $options['show_behavior_summary'] : TRUE;
    $depth_limit = isset($options['depth_limit']) ? $options['depth_limit'] : 1;

    $summary['content'][] = $this->get('title')->value;

    if ($this->hasField('agenda_items')) {
      // Decrease the depth, since we are entering a nested paragraph.
      $nested_summary = $this->getNestedSummary('agenda_items', [
        'show_behavior_summary' => FALSE,
        'depth_limit' => $depth_limit - 1
      ]);
      $summary['content'] = array_merge($summary['content'], $nested_summary);
    }

    return $summary;
  }

}
