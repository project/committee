<?php

namespace Drupal\committee_agenda\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Agenda entities.
 *
 * @ingroup committee_agenda
 */
interface AgendaInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Gets the parent entity of the paragraph.
   *
   * Preserves language context with translated entities.
   *
   * @return ContentEntityInterface
   *   The parent entity.
   */
  public function getParentEntity();

  /**
   * Set the parent entity of the paragraph.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $parent
   *   The parent entity.
   * @param string $parent_field_name
   *   The parent field name.
   *
   * @return $this
   */
  public function setParentEntity(ContentEntityInterface $parent, $parent_field_name);

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Agenda creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Agenda.
   */
  public function getCreatedTime();

  /**
   * Sets the Agenda creation timestamp.
   *
   * @param int $timestamp
   *   The Agenda creation timestamp.
   *
   * @return \Drupal\committee_agenda\Entity\AgendaInterface
   *   The called Agenda entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Agenda revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Agenda revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\committee_agenda\Entity\AgendaInterface
   *   The called Agenda entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Agenda revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Agenda revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\committee_agenda\Entity\AgendaInterface
   *   The called Agenda entity.
   */
  public function setRevisionUserId($uid);

}
