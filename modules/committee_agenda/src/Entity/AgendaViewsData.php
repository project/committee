<?php

namespace Drupal\committee_agenda\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Agenda entities.
 */
class AgendaViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
