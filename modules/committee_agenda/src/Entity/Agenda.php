<?php

namespace Drupal\committee_agenda\Entity;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\TypedData\TranslatableInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\user\EntityOwnerTrait;
use Drupal\user\UserInterface;

/**
 * Defines the Agenda entity.
 *
 * @ingroup committee_agenda
 *
 * @ContentEntityType(
 *   id = "committee_agenda",
 *   label = @Translation("Agenda"),
 *   handlers = {
 *     "storage" = "Drupal\committee_agenda\Storage\AgendaStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\committee_agenda\AgendaListBuilder",
 *     "views_data" = "Drupal\committee_agenda\Entity\AgendaViewsData",
 *     "translation" = "Drupal\committee_agenda\AgendaTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\committee_agenda\Form\AgendaForm",
 *       "add" = "Drupal\committee_agenda\Form\AgendaForm",
 *       "edit" = "Drupal\committee_agenda\Form\AgendaForm",
 *       "delete" = "Drupal\committee_agenda\Form\AgendaDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\committee_agenda\Routing\AgendaHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\committee_agenda\Access\AgendaAccessControlHandler",
 *   },
 *   base_table = "committee_agenda",
 *   data_table = "committee_agenda_field_data",
 *   revision_table = "committee_agenda_revision",
 *   revision_data_table = "committee_agenda_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer agenda entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "owner" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "canonical" = "/committee/agenda/{committee_agenda}",
 *     "add-form" = "/committee/agenda/add",
 *     "edit-form" = "/committee/agenda/{committee_agenda}/edit",
 *     "delete-form" = "/committee/agenda/{committee_agenda}/delete",
 *     "version-history" = "/committee/agenda/{committee_agenda}/revisions",
 *     "revision" = "/committee/agenda/{committee_agenda}/revisions/{committee_agenda_revision}/view",
 *     "revision_revert" = "/committee/agenda/{committee_agenda}/revisions/{committee_agenda_revision}/revert",
 *     "revision_delete" = "/committee/agenda/{committee_agenda}/revisions/{committee_agenda_revision}/delete",
 *     "translation_revert" = "/committee/agenda/{committee_agenda}/revisions/{committee_agenda_revision}/revert/{langcode}",
 *   },
 *   field_ui_base_route = "committee_agenda.settings"
 * )
 */
class Agenda extends EditorialContentEntityBase implements AgendaInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the committee_agenda owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParentEntity() {
    if (!isset($this->get('parent_type')->value) || !isset($this->get('parent_id')->value)) {
      return NULL;
    }

    $parent = \Drupal::entityTypeManager()->getStorage($this->get('parent_type')->value)->load($this->get('parent_id')->value);

    // Return current translation of parent entity, if it exists.
    if ($parent != NULL && ($parent instanceof TranslatableInterface) && $parent->hasTranslation($this->language()->getId())) {
      return $parent->getTranslation($this->language()->getId());
    }

    return $parent;
  }

  /**
   * {@inheritdoc}
   */
  public function setParentEntity(ContentEntityInterface $parent, $parent_field_name) {
    $this->set('parent_type', $parent->getEntityTypeId());
    $this->set('parent_id', $parent->id());
    $this->set('parent_field_name', $parent_field_name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
//    if (($parent = $this->getParentEntity()) && $parent->hasField($this->get('parent_field_name')->value)) {
//      $parent_field = $this->get('parent_field_name')->value;
//      $field = $parent->get($parent_field);
//      $found = FALSE;
//      foreach ($field as $key => $value) {
//        if ($value->entity->id() == $this->id()) {
//          $found = TRUE;
//          break;
//        }
//      }
//      if ($found) {
//        $label = $parent->label() . ' > ' . $field->getFieldDefinition()->getLabel();
//      } else {
//        // A previous or draft revision or a deleted stale Paragraph.
//        $label = $parent->label() . ' > ' . $field->getFieldDefinition()->getLabel() . ' (previous revision)';
//      }
//    }
//    else {
//      $label = t('Orphaned @type: @summary', ['@summary' => Unicode::truncate(strip_tags($this->getSummary()), 50, FALSE, TRUE), '@type' => $this->get('type')->entity->label()]);
//    }
    $label = t('Agenda') . $this->id();
    return $label;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    $fields['parent_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Parent ID'))
      ->setDescription(t('The ID of the parent entity of which this entity is referenced.'))
      ->setSetting('is_ascii', TRUE)
      ->setRevisionable(TRUE);

    $fields['parent_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Parent type'))
      ->setDescription(t('The entity parent type to which this entity is referenced.'))
      ->setSetting('is_ascii', TRUE)
      ->setSetting('max_length', EntityTypeInterface::ID_MAX_LENGTH)
      ->setRevisionable(TRUE);

    $fields['parent_field_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Parent field name'))
      ->setDescription(t('The entity parent field name to which this entity is referenced.'))
      ->setSetting('is_ascii', TRUE)
      ->setSetting('max_length', FieldStorageConfig::NAME_MAX_LENGTH)
      ->setRevisionable(TRUE);

    $fields['agenda_items'] = BaseFieldDefinition::create('entity_reference_hierarchy_revisions')
      ->setLabel(t('Agenda Items'))
      ->setDescription(t('Items for the agenda'))
      ->setSetting('target_type', 'committee_agenda_item')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_hierarchy_ief',
        'weight' => 5,
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_hierarchy_revisions_entity_view',
        'label' => 'hidden',
        'weight' => 0,
        'settings' => [
          'view_mode' => 'default',
          'link' => FALSE,
        ]
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function createDuplicate() {
    $duplicate = parent::createDuplicate();
    // Loop over entity fields and duplicate nested paragraphs.
    foreach ($duplicate->getFields() as $fieldItemList) {
      if ($fieldItemList instanceof EntityReferenceFieldItemListInterface && $fieldItemList->getSetting('target_type') === $this->getEntityTypeId()) {
        foreach ($fieldItemList as $delta => $item) {
          if ($item->entity) {
            $fieldItemList[$delta] = $item->entity->createDuplicate();
          }
        }
      }
    }
    return $duplicate;
  }

}
