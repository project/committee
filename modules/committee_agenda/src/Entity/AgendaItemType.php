<?php

namespace Drupal\committee_agenda\Entity;

use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphsTypeInterface;

/**
 * Defines the Committee agenda item type entity.
 *
 * @ConfigEntityType(
 *   id = "committee_agenda_item_type",
 *   label = @Translation("Agenda item type"),
 *   label_collection = @Translation("Agenda item types"),
 *   label_singular = @Translation("Agenda item type"),
 *   label_plural = @Translation("Agenda item types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Agenda item type",
 *     plural = "@count Agenda item types",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\paragraphs\ParagraphsTypeAccessControlHandler",
 *     "list_builder" = "Drupal\paragraphs\Controller\ParagraphsTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\committee_agenda\Form\AgendaItemTypeForm",
 *       "edit" = "Drupal\committee_agenda\Form\AgendaItemTypeForm",
 *       "delete" = "Drupal\paragraphs\Form\ParagraphsTypeDeleteConfirm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "committee_agenda_item_type",
 *   admin_permission = "administer agenda item types",
 *   bundle_of = "committee_agenda_item",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "icon_uuid",
 *     "icon_default",
 *     "description",
 *     "behavior_plugins",
 *   },
 *   links = {
 *     "canonical" = "/admin/committee/settings/agenda-item/type/{committee_agenda_item_type}",
 *     "add-form" = "/admin/committee/settings/agenda-item/type/add",
 *     "edit-form" = "/admin/committee/settings/agenda-item/type/{committee_agenda_item_type}/edit",
 *     "delete-form" = "/admin/committee/settings/agenda-item/type/{committee_agenda_item_type}/delete",
 *     "collection" = "/admin/committee/settings/agenda-item/type"
 *   }
 * )
 */
class AgendaItemType extends ParagraphsType implements AgendaItemTypeInterface {

}
