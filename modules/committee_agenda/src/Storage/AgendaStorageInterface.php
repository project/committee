<?php

namespace Drupal\committee_agenda\Storage;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\committee_agenda\Entity\AgendaInterface;

/**
 * Defines the storage handler class for Agenda entities.
 *
 * This extends the base storage class, adding required special handling for
 * Agenda entities.
 *
 * @ingroup committee_agenda
 */
interface AgendaStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Agenda revision IDs for a specific Agenda.
   *
   * @param \Drupal\committee_agenda\Entity\AgendaInterface $entity
   *   The Agenda entity.
   *
   * @return int[]
   *   Agenda revision IDs (in ascending order).
   */
  public function revisionIds(AgendaInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Agenda author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Agenda revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\committee_agenda\Entity\AgendaInterface $entity
   *   The Agenda entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(AgendaInterface $entity);

  /**
   * Unsets the language for all Agenda with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
