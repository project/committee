<?php

namespace Drupal\committee_agenda\Storage;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\committee_agenda\Entity\AgendaInterface;

/**
 * Defines the storage handler class for Agenda entities.
 *
 * This extends the base storage class, adding required special handling for
 * Agenda entities.
 *
 * @ingroup committee_agenda
 */
class AgendaStorage extends SqlContentEntityStorage implements AgendaStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(AgendaInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {committee_agenda_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {committee_agenda_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(AgendaInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {committee_agenda_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('committee_agenda_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
