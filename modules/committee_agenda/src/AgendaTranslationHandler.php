<?php

namespace Drupal\committee_agenda;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for committee_agenda.
 */
class AgendaTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
