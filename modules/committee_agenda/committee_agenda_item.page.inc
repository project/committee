<?php

/**
 * @file
 * Contains committee_agenda_item.page.inc.
 *
 * Page callback for Committee agenda item entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Committee agenda item templates.
 *
 * Default template: committee_agenda_item.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_committee_agenda_item(array &$variables) {
  // Fetch AgendaItem Entity Object.
  $committee_agenda_item = $variables['elements']['#committee_agenda_item'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
