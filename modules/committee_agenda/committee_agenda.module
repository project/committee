<?php

/**
 * @file
 * Contains committee_agenda.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BundleFieldDefinition;
use Drupal\Core\Field\FieldDefinition;

/**
 * Implements hook_help().
 */
function committee_agenda_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the committee_agenda module.
    case 'help.page.committee_agenda':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Extend Committee Module with Agenda Builder Functionality') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_entity_field_storage_info().
 */
function committee_agenda_entity_field_storage_info(EntityTypeInterface $entity_type) {
  $fields = [];

  if($entity_type->id() == 'committee_meeting') {
    $fields['agenda'] = BundleFieldDefinition::create('entity_reference')
      ->setTargetEntityTypeId($entity_type->id())
      ->setName('agenda')
      ->setSetting('target_type', 'committee_agenda')
      ->setCardinality(1);
  }

  if($entity_type->id() == 'committee_agenda_item') {
    $fields['minutes_meeting_id'] = BundleFieldDefinition::create('entity_reference')
      ->setTargetEntityTypeId($entity_type->id())
      ->setName('minutes_meeting_id')
      ->setSetting('target_type', 'committee_meeting')
      ->setCardinality(1);
  }

  return $fields;
}

/**
 * Implements hook_entity_bundle_field_info().
 */
function committee_agenda_entity_bundle_field_info(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
  $fields = [];
  $storage_definitions = committee_agenda_entity_field_storage_info($entity_type);

  if($entity_type->id() == "committee_meeting") {
    $fields['agenda'] = FieldDefinition::createFromFieldStorageDefinition($storage_definitions['agenda'])
      ->setLabel(t('Agenda'))
      ->setDescription(t('The Agenda'))
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_simple',
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'label' => 'hidden',
        'weight' => 10,
        'settings' => [
          'view_mode' => 'default',
          'link' => FALSE,
        ]
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
  }

  if($entity_type->id() == "committee_agenda_item" && $bundle == "minutes") {
    $fields['minutes_meeting_id'] = FieldDefinition::createFromFieldStorageDefinition($storage_definitions['minutes_meeting_id'])
      ->setLabel(t('Meeting'))
      ->setDescription(t('The Meeting Minutes to Approve'))

      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
  }

  return $fields;
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function  committee_agenda_committee_meeting_insert(\Drupal\committee_meeting\Entity\CommitteeMeetingInterface $meeting) {
  committee_agenda_committee_meeting_update($meeting);
}

/**
 * Implements hook_ENTITY_TYPE_update().
 */
function  committee_agenda_committee_meeting_update(\Drupal\committee_meeting\Entity\CommitteeMeetingInterface $meeting) {
  /** @var \Drupal\committee_agenda\Entity\Agenda $agenda */
  if ($agenda = $meeting->agenda->entity) {
    $agenda->setParentEntity($meeting, 'agenda');
    $agenda->save();
  }
}

/**
 * Implements hook_theme().
 */
function committee_agenda_theme() {
  $theme = [];
  $theme['committee_agenda'] = [
    'render element' => 'elements',
    'file' => 'committee_agenda.page.inc',
    'template' => 'committee_agenda',
  ];
  $theme['committee_agenda_content_add_list'] = [
    'render element' => 'content',
    'variables' => ['content' => NULL],
    'file' => 'committee_agenda.page.inc',
  ];

  $theme['committee_agenda_item'] = [
    'render element' => 'elements',
    'file' => 'committee_agenda_item.page.inc',
    'template' => 'committee_agenda_item',
  ];
  $theme['committee_agenda_item_content_add_list'] = [
    'render element' => 'content',
    'variables' => ['content' => NULL],
    'file' => 'committee_agenda_item.page.inc',
  ];
  return $theme;
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function committee_agenda_theme_suggestions_committee_agenda(array $variables) {
  $suggestions = [];

  $entity = $variables['elements']['#committee_agenda'];
  $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'committee_agenda__' . $sanitized_view_mode;
  $suggestions[] = 'committee_agenda__' . $entity->bundle();
  $suggestions[] = 'committee_agenda__' . $entity->bundle() . '__' . $sanitized_view_mode;
  $suggestions[] = 'committee_agenda__' . $entity->id();
  $suggestions[] = 'committee_agenda__' . $entity->id() . '__' . $sanitized_view_mode;

  return $suggestions;
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function committee_agenda_theme_suggestions_committee_agenda_item(array $variables) {
  $suggestions = [];

  $entity = $variables['elements']['#committee_agenda_item'];
  $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'committee_agenda_item__' . $sanitized_view_mode;
  $suggestions[] = 'committee_agenda_item__' . $entity->bundle();
  $suggestions[] = 'committee_agenda_item__' . $entity->bundle() . '__' . $sanitized_view_mode;
  $suggestions[] = 'committee_agenda_item__' . $entity->id();
  $suggestions[] = 'committee_agenda_item__' . $entity->id() . '__' . $sanitized_view_mode;

  return $suggestions;
}
