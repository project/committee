<?php

/**
 * @file
 * Contains committee_agenda.page.inc.
 *
 * Page callback for Agenda entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Agenda templates.
 *
 * Default template: committee_agenda.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_committee_agenda(array &$variables) {
  // Fetch Agenda Entity Object.
  $committee_agenda = $variables['elements']['#committee_agenda'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
