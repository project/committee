<?php

/**
 * @file
 * Contains committee_attendance.module.
 */

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldDefinition;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Field\BundleFieldDefinition;

/**
 * Implements hook_help().
 */
function committee_attendance_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the committee_attendance module.
    case 'help.page.committee_attendance':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Record Attendance at Committee Meetings') . '</p>';
      return $output;

    default:
  }
}


/**
 * Implements hook_entity_field_storage_info().
 *
 * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
 *
 * @return array
 */
function committee_attendance_entity_field_storage_info(EntityTypeInterface $entity_type) {
  $fields = [];

  if($entity_type->id() == 'committee_meeting') {
    $fields['attendees'] = BundleFieldDefinition::create('entity_reference')
      ->setTargetEntityTypeId($entity_type->id())
      ->setName('attendees')
      ->setSetting('target_type', 'committee_attendee')
      ->setCardinality(BundleFieldDefinition::CARDINALITY_UNLIMITED);
  }

  return $fields;
}

/**
 * Implements hook_entity_bundle_field_info().
 *
 * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
 * @param $bundle
 * @param array $base_field_definitions
 *
 * @return array
 */
function committee_attendance_entity_bundle_field_info(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
  $fields = [];

  if($entity_type->id() == "committee_meeting") {

    $storage_definitions = committee_attendance_entity_field_storage_info($entity_type);

    $fields['attendees'] = FieldDefinition::createFromFieldStorageDefinition($storage_definitions['attendees'])
      ->setLabel(t('Attendees'))
      ->setDescription(t('The Meeting Attendees'))
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_simple',
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'label' => 'hidden',
        'weight' => 20,
        'settings' => [
          'view_mode' => 'default',
          'link' => FALSE,
        ]
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
  }

  return $fields;
}
