<?php

namespace Drupal\committee_attendance\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Attendee entity.
 *
 * @ingroup committee_attendance
 *
 * @ContentEntityType(
 *   id = "committee_attendee",
 *   label = @Translation("Attendee"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\committee_attendance\AttendeeListBuilder",
 *     "views_data" = "Drupal\committee_attendance\Entity\AttendeeViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\committee_attendance\Form\AttendeeForm",
 *       "add" = "Drupal\committee_attendance\Form\AttendeeForm",
 *       "edit" = "Drupal\committee_attendance\Form\AttendeeForm",
 *       "delete" = "Drupal\committee_attendance\Form\AttendeeDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\committee_attendance\AttendeeHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\committee_attendance\AttendeeAccessControlHandler",
 *   },
 *   base_table = "committee_attendee",
 *   translatable = FALSE,
 *   admin_permission = "administer attendee entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "langcode" = "langcode"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/committee_attendee/{committee_attendee}",
 *     "add-form" = "/admin/structure/committee_attendee/add",
 *     "edit-form" = "/admin/structure/committee_attendee/{committee_attendee}/edit",
 *     "delete-form" = "/admin/structure/committee_attendee/{committee_attendee}/delete",
 *     "collection" = "/admin/structure/committee_attendee",
 *   },
 *   field_ui_base_route = "committee_attendee.settings"
 * )
 */
class Attendee extends ContentEntityBase implements AttendeeInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Attendee entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['member'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel('Member')
      ->setDescription('The contact who is a member of the committee')
      ->setSetting('target_type', 'user')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
