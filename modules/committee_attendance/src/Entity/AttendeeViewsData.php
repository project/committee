<?php

namespace Drupal\committee_attendance\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Attendee entities.
 */
class AttendeeViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
