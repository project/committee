<?php

namespace Drupal\committee_attendance\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Attendee entities.
 *
 * @ingroup committee_attendance
 */
interface AttendeeInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Attendee name.
   *
   * @return string
   *   Name of the Attendee.
   */
  public function getName();

  /**
   * Sets the Attendee name.
   *
   * @param string $name
   *   The Attendee name.
   *
   * @return \Drupal\committee_attendance\Entity\AttendeeInterface
   *   The called Attendee entity.
   */
  public function setName($name);

  /**
   * Gets the Attendee creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Attendee.
   */
  public function getCreatedTime();

  /**
   * Sets the Attendee creation timestamp.
   *
   * @param int $timestamp
   *   The Attendee creation timestamp.
   *
   * @return \Drupal\committee_attendance\Entity\AttendeeInterface
   *   The called Attendee entity.
   */
  public function setCreatedTime($timestamp);

}
