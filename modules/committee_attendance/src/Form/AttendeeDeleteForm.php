<?php

namespace Drupal\committee_attendance\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Attendee entities.
 *
 * @ingroup committee_attendance
 */
class AttendeeDeleteForm extends ContentEntityDeleteForm {


}
