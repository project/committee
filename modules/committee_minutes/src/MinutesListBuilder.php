<?php

namespace Drupal\committee_minutes;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Minutes entities.
 *
 * @ingroup committee_minutes
 */
class MinutesListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Minutes ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\committee_minutes\Entity\Minutes $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.committee_minutes.edit_form',
      ['committee_minutes' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
