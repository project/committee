<?php

namespace Drupal\committee_minutes\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Minute item entities.
 *
 * @ingroup committee
 */
class MinuteItemDeleteForm extends ContentEntityDeleteForm {


}
