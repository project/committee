<?php

namespace Drupal\committee_minutes\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Minutes revision.
 *
 * @ingroup committee_minutes
 */
class MinutesRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Minutes revision.
   *
   * @var \Drupal\committee_minutes\Entity\MinutesInterface
   */
  protected $revision;

  /**
   * The Minutes storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $minutesStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->minutesStorage = $container->get('entity_type.manager')->getStorage('committee_minutes');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'committee_minutes_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.committee_minutes.version_history', ['committee_minutes' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $committee_minutes_revision = NULL) {
    $this->revision = $this->MinutesStorage->loadRevision($committee_minutes_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->MinutesStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Minutes: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage(t('Revision from %revision-date of Minutes %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.committee_minutes.canonical',
       ['committee_minutes' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {committee_minutes_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.committee_minutes.version_history',
         ['committee_minutes' => $this->revision->id()]
      );
    }
  }

}
