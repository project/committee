<?php

namespace Drupal\committee_minutes\Form;

use Drupal\committee\Entity\CommitteeInterface;
use Drupal\committee_minutes\Entity\MinutesInterface;
use Drupal\committee_meeting\Entity\CommitteeMeetingInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Committee Meeting Minutess form.
 */
class MeetingMinutesForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'committee_minutes_meeting_minutes';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, CommitteeInterface $committee = NULL, CommitteeMeetingInterface $committee_meeting = NULL) {

    /** @var MinutesInterface $minutes */
    $minutes = !$committee_meeting->get('minutes')->isEmpty() ?  $committee_meeting->minutes->entity : NULL;
    $form_state->set('committee', $committee);
    $form_state->set('committee_meeting', $committee_meeting);

    $form['minutes'] = [
      '#type' => 'inline_entity_form',
      '#entity_type' => 'committee_minutes',
      '#default_value' => $minutes,
    ];

    // Add a submit button. Give it a class for easy JavaScript targeting.
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var MinutesInterface $minutes */
    $minutes = $form['minutes']['#entity'];
    $minutes_items = $minutes->get('minutes_items');
    /** @var EntityReferenceItem $minutes_item */
    foreach ($minutes_items as $minutes_item) {
      \Drupal::logger('committee_minutes')->notice(print_r($minutes_item->getValue(), TRUE));
    }

    $minutes->save();

    /** @var CommitteeMeetingInterface $committee */
    $committee = $form_state->get('committee');
    /** @var CommitteeMeetingInterface $committee_meeting */
    $committee_meeting = $form_state->get('committee_meeting');

    if($committee_meeting->get('minutes')->isEmpty()) {
      $committee_meeting->get('minutes')->setValue($minutes->id());
      $committee_meeting->save();
    }

    $form_state->setRedirect('entity.committee_meeting.committee_minutes', [
      'committee' => $committee->id(),
      'committee_meeting' => $committee_meeting->id(),
    ]);
  }

}
