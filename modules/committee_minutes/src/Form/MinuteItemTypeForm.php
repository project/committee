<?php

namespace Drupal\committee_minutes\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MinuteItemTypeForm.
 */
class MinuteItemTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $committee_minute_item_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $committee_minute_item_type->label(),
      '#description' => $this->t("Label for the Minute item type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $committee_minute_item_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\committee_minutes\Entity\MinuteItemType::load',
      ],
      '#disabled' => !$committee_minute_item_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $committee_minute_item_type = $this->entity;
    $status = $committee_minute_item_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Minute item type.', [
          '%label' => $committee_minute_item_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Minute item type.', [
          '%label' => $committee_minute_item_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($committee_minute_item_type->toUrl('collection'));
  }

}
