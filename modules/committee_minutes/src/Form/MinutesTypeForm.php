<?php

namespace Drupal\committee_minutes\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MinutesTypeForm.
 */
class MinutesTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $committee_minutes_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $committee_minutes_type->label(),
      '#description' => $this->t("Label for the Minutes type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $committee_minutes_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\committee_minutes\Entity\MinutesType::load',
      ],
      '#disabled' => !$committee_minutes_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $committee_minutes_type = $this->entity;
    $status = $committee_minutes_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Minutes type.', [
          '%label' => $committee_minutes_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Minutes type.', [
          '%label' => $committee_minutes_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($committee_minutes_type->toUrl('collection'));
  }

}
