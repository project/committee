<?php

namespace Drupal\committee_minutes\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Minutes entities.
 *
 * @ingroup committee_minutes
 */
class MinutesDeleteForm extends ContentEntityDeleteForm {


}
