<?php

namespace Drupal\committee_minutes;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for committee_minutes.
 */
class MinutesTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
