<?php

namespace Drupal\committee_minutes\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\user\EntityOwnerTrait;
use Drupal\user\UserInterface;

/**
 * Defines the minute item entity.
 *
 * @ingroup committee
 *
 * @ContentEntityType(
 *   id = "committee_minute_item",
 *   label = @Translation("Minute item"),
 *   label_collection = @Translation("Minute items"),
 *   label_singular = @Translation("Minute item"),
 *   label_plural = @Translation("Minute items"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Minute Item",
 *     plural = "@count Minute Items",
 *   ),
 *   bundle_label = @Translation("Minute item type"),
 *   handlers = {
 *     "view_builder" = "Drupal\paragraphs\ParagraphViewBuilder",
 *     "access" = "Drupal\committee_minutes\Access\MinuteItemAccessControlHandler",
 *     "storage_schema" = "Drupal\paragraphs\ParagraphStorageSchema",
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm"
 *     },
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   base_table = "committee_minute_item",
 *   data_table = "committee_minute_item_field_data",
 *   revision_table = "committee_minute_item_revision",
 *   revision_data_table = "committee_minute_item_revision_field_data",
 *   translatable = TRUE,
 *   entity_revision_parent_type_field = "parent_type",
 *   entity_revision_parent_id_field = "parent_id",
 *   entity_revision_parent_field_name_field = "parent_field_name",
 *   admin_permission = "administer committee minute item entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "bundle" = "type",
 *     "langcode" = "langcode",
 *     "revision" = "revision_id",
 *     "published" = "status"
 *   },
 *   bundle_entity_type = "committee_minute_item_type",
 *   field_ui_base_route = "entity.committee_minute_item_type.edit_form",
 *   common_reference_revisions_target = TRUE,
 *   content_translation_ui_skip = TRUE,
 *   render_cache = FALSE,
 *   default_reference_revision_settings = {
 *     "field_storage_config" = {
 *       "cardinality" = -1,
 *       "settings" = {
 *         "target_type" = "committee_minute_item"
 *       }
 *     },
 *     "field_config" = {
 *       "settings" = {
 *         "handler" = "default:committee_minute_item"
 *       }
 *     },
 *     "entity_form_display" = {
 *       "type" = "entity_reference_paragraphs"
 *     },
 *     "entity_view_display" = {
 *       "type" = "entity_reference_revisions_entity_view"
 *     }
 *   },
 *   serialized_field_property_names = {
 *     "behavior_settings" = {
 *       "value"
 *     }
 *   }
 * )
 */
class MinuteItem extends Paragraph implements MinuteItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   *
   * The Paragraph getSummaryItems doesn't use base fields. So here we override
   * to use out fields in AgendaItems
   */
  public function getSummaryItems(array $options = []) {
    $summary = ['content' => [], 'behaviors' => []];
    $show_behavior_summary = isset($options['show_behavior_summary']) ? $options['show_behavior_summary'] : TRUE;
    $depth_limit = isset($options['depth_limit']) ? $options['depth_limit'] : 1;

    $summary['content'][] = $this->get('title')->value;

    if ($this->hasField('agenda_items')) {
      // Decrease the depth, since we are entering a nested paragraph.
      $nested_summary = $this->getNestedSummary('agenda_items', [
        'show_behavior_summary' => FALSE,
        'depth_limit' => $depth_limit - 1
      ]);
      $summary['content'] = array_merge($summary['content'], $nested_summary);
    }

    return $summary;
  }

}
