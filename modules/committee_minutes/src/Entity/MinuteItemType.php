<?php

namespace Drupal\committee_minutes\Entity;

use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphsTypeInterface;

/**
 * Defines the Minute item type entity.
 *
 * @ConfigEntityType(
 *   id = "committee_minute_item_type",
 *   label = @Translation("Minute item type"),
 *   handlers = {
 *     "access" = "Drupal\paragraphs\ParagraphsTypeAccessControlHandler",
 *     "list_builder" = "Drupal\paragraphs\Controller\ParagraphsTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\paragraphs\Form\ParagraphsTypeForm",
 *       "edit" = "Drupal\paragraphs\Form\ParagraphsTypeForm",
 *       "delete" = "Drupal\paragraphs\Form\ParagraphsTypeDeleteConfirm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\committee_minutes\Routing\MinuteItemTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "committee_minute_item_type",
 *   admin_permission = "administer minute item types",
 *   bundle_of = "committee_minute_item",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "icon_uuid",
 *     "icon_default",
 *     "description",
 *     "behavior_plugins",
 *   },
 *   links = {
 *     "canonical" = "/admin/committee/settings/minute_item_type/{committee_minute_item_type}",
 *     "add-form" = "/admin/committee/settings/minute_item_type/add",
 *     "edit-form" = "/admin/committee/settings/minute_item_type/{committee_minute_item_type}/edit",
 *     "delete-form" = "/admin/committee/settings/minute_item_type/{committee_minute_item_type}/delete",
 *     "collection" = "/admin/committee/settings/minute_item_type"
 *   }
 * )
 */
class MinuteItemType extends ParagraphsType implements ParagraphsTypeInterface, EntityWithPluginCollectionInterface {

}
