<?php

namespace Drupal\committee_minutes\Entity;

use Drupal\paragraphs\ParagraphInterface;

/**
 * Provides an interface for defining Minute item entities.
 *
 * @ingroup committee
 */
interface MinuteItemInterface extends ParagraphInterface {

}
