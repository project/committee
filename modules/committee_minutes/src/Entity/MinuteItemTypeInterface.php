<?php

namespace Drupal\committee_minutes\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Minute item type entities.
 */
interface MinuteItemTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
