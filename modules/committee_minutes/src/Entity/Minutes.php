<?php

namespace Drupal\committee_minutes\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\TypedData\TranslatableInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\user\EntityOwnerTrait;
use Drupal\user\UserInterface;

/**
 * Defines the Minutes entity.
 *
 * @ingroup committee_minutes
 *
 * @ContentEntityType(
 *   id = "committee_minutes",
 *   label = @Translation("Minutes"),
 *   bundle_label = @Translation("Minutes type"),
 *   handlers = {
 *     "storage" = "Drupal\committee_minutes\Storage\MinutesStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\committee_minutes\MinutesListBuilder",
 *     "views_data" = "Drupal\committee_minutes\Entity\MinutesViewsData",
 *     "translation" = "Drupal\committee_minutes\MinutesTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\committee_minutes\Form\MinutesForm",
 *       "add" = "Drupal\committee_minutes\Form\MinutesForm",
 *       "edit" = "Drupal\committee_minutes\Form\MinutesForm",
 *       "delete" = "Drupal\committee_minutes\Form\MinutesDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\committee_minutes\Routing\MinutesHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\committee_minutes\Access\MinutesAccessControlHandler",
 *   },
 *   base_table = "committee_minutes",
 *   data_table = "committee_minutes_field_data",
 *   revision_table = "committee_minutes_revision",
 *   revision_data_table = "committee_minutes_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer minutes entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "owner" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "canonical" = "/admin/committee/committee_minutes/{committee_minutes}",
 *     "add-page" = "/admin/committee/committee_minutes/add",
 *     "add-form" = "/admin/committee/committee_minutes/add/{committee_minutes_type}",
 *     "edit-form" = "/admin/committee/committee_minutes/{committee_minutes}/edit",
 *     "delete-form" = "/admin/committee/committee_minutes/{committee_minutes}/delete",
 *     "version-history" = "/admin/committee/committee_minutes/{committee_minutes}/revisions",
 *     "revision" = "/admin/committee/committee_minutes/{committee_minutes}/revisions/{committee_minutes_revision}/view",
 *     "revision_revert" = "/admin/committee/committee_minutes/{committee_minutes}/revisions/{committee_minutes_revision}/revert",
 *     "revision_delete" = "/admin/committee/committee_minutes/{committee_minutes}/revisions/{committee_minutes_revision}/delete",
 *     "translation_revert" = "/admin/committee/committee_minutes/{committee_minutes}/revisions/{committee_minutes_revision}/revert/{langcode}",
 *     "collection" = "/admin/committee/committee_minutes",
 *   },
 *   field_ui_base_route = "committee_minutes.settings"
 * )
 */
class Minutes extends EditorialContentEntityBase implements MinutesInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the committee_minutes owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParentEntity() {
    if (!isset($this->get('parent_type')->value) || !isset($this->get('parent_id')->value)) {
      return NULL;
    }

    $parent = \Drupal::entityTypeManager()->getStorage($this->get('parent_type')->value)->load($this->get('parent_id')->value);

    // Return current translation of parent entity, if it exists.
    if ($parent != NULL && ($parent instanceof TranslatableInterface) && $parent->hasTranslation($this->language()->getId())) {
      return $parent->getTranslation($this->language()->getId());
    }

    return $parent;
  }

  /**
   * {@inheritdoc}
   */
  public function setParentEntity(ContentEntityInterface $parent, $parent_field_name) {
    $this->set('parent_type', $parent->getEntityTypeId());
    $this->set('parent_id', $parent->id());
    $this->set('parent_field_name', $parent_field_name);
    return $this;
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    $fields['parent_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Parent ID'))
      ->setDescription(t('The ID of the parent entity of which this entity is referenced.'))
      ->setSetting('is_ascii', TRUE)
      ->setRevisionable(TRUE);

    $fields['parent_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Parent type'))
      ->setDescription(t('The entity parent type to which this entity is referenced.'))
      ->setSetting('is_ascii', TRUE)
      ->setSetting('max_length', EntityTypeInterface::ID_MAX_LENGTH)
      ->setRevisionable(TRUE);

    $fields['parent_field_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Parent field name'))
      ->setDescription(t('The entity parent field name to which this entity is referenced.'))
      ->setSetting('is_ascii', TRUE)
      ->setSetting('max_length', FieldStorageConfig::NAME_MAX_LENGTH)
      ->setRevisionable(TRUE);

    $fields['minute_items'] = BaseFieldDefinition::create('entity_reference_hierarchy_revisions')
      ->setLabel(t('Minute Items'))
      ->setDescription(t('Items for the minutes'))
      ->setSetting('target_type', 'committee_minute_item')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_hierarchy_ief',
        'weight' => 5,
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_hierarchy_revisions_entity_view',
        'label' => 'hidden',
        'weight' => 0,
        'settings' => [
          'view_mode' => 'default',
          'link' => FALSE,
        ]
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
