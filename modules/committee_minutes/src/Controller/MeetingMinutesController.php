<?php

namespace Drupal\committee_minutes\Controller;

use Drupal\committee\Entity\CommitteeInterface;
use Drupal\committee_minutes\Entity\MinutesInterface;
use Drupal\committee_meeting\Entity\CommitteeMeetingInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Committee Meeting Minutess routes.
 */
class MeetingMinutesController extends ControllerBase {

  /**
   * Builds the response.
   * @param CommitteeInterface|null $committee
   * @param CommitteeMeetingInterface|null $committee_meeting
   * @return mixed
   */
  public function build(CommitteeInterface $committee = NULL, CommitteeMeetingInterface $committee_meeting = NULL) {

    if (!$committee_meeting->get('minutes')->isEmpty()) {
      /** @var MinutesInterface $minutes */
      $minutes = $committee_meeting->minutes->entity;

      $view_builder = $this->entityTypeManager()->getViewBuilder($minutes->getEntityTypeId());

      return $view_builder->view($minutes);
    }
    else {
      return [
        '#markup' => "No Minutes Yet",
        '#cache' => [
          'keys' => ['entity_view', 'committee_meeting', $committee_meeting->id()],
          'contexts' => ['languages'],
          'tags' => ['committee_meeting:' . $committee_meeting->id()],
          'max-age' => Cache::PERMANENT,
        ],
      ];
    }

  }

  /**
   * Return the Page title.
   * @param CommitteeInterface|null $committee
   * @param CommitteeMeetingInterface|null $committee_meeting
   * @return mixed
   */
  public function getTitle(CommitteeInterface $committee = NULL, CommitteeMeetingInterface $committee_meeting = NULL) {

    return $committee_meeting->label() . " Minutes";

  }

}
