<?php

namespace Drupal\committee_minutes\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\committee_minutes\Entity\MinutesInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MinutesController.
 *
 *  Returns responses for Minutes routes.
 */
class MinutesController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Minutes revision.
   *
   * @param int $committee_minutes_revision
   *   The Minutes revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($committee_minutes_revision) {
    $committee_minutes = $this->entityTypeManager()->getStorage('committee_minutes')
      ->loadRevision($committee_minutes_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('committee_minutes');

    return $view_builder->view($committee_minutes);
  }

  /**
   * Page title callback for a Minutes revision.
   *
   * @param int $committee_minutes_revision
   *   The Minutes revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($committee_minutes_revision) {
    $committee_minutes = $this->entityTypeManager()->getStorage('committee_minutes')
      ->loadRevision($committee_minutes_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $committee_minutes->label(),
      '%date' => $this->dateFormatter->format($committee_minutes->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Minutes.
   *
   * @param \Drupal\committee_minutes\Entity\MinutesInterface $committee_minutes
   *   A Minutes object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(MinutesInterface $committee_minutes) {
    $account = $this->currentUser();
    $committee_minutes_storage = $this->entityTypeManager()->getStorage('committee_minutes');

    $langcode = $committee_minutes->language()->getId();
    $langname = $committee_minutes->language()->getName();
    $languages = $committee_minutes->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $committee_minutes->label()]) : $this->t('Revisions for %title', ['%title' => $committee_minutes->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all minutes revisions") || $account->hasPermission('administer minutes entities')));
    $delete_permission = (($account->hasPermission("delete all minutes revisions") || $account->hasPermission('administer minutes entities')));

    $rows = [];

    $vids = $committee_minutes_storage->revisionIds($committee_minutes);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\committee_minutes\MinutesInterface $revision */
      $revision = $committee_minutes_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $committee_minutes->getRevisionId()) {
          $link = $this->l($date, new Url('entity.committee_minutes.revision', [
            'committee_minutes' => $committee_minutes->id(),
            'committee_minutes_revision' => $vid,
          ]));
        }
        else {
          $link = $committee_minutes->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.committee_minutes.translation_revert', [
                'committee_minutes' => $committee_minutes->id(),
                'committee_minutes_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.committee_minutes.revision_revert', [
                'committee_minutes' => $committee_minutes->id(),
                'committee_minutes_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.committee_minutes.revision_delete', [
                'committee_minutes' => $committee_minutes->id(),
                'committee_minutes_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['committee_minutes_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
