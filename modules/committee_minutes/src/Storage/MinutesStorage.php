<?php

namespace Drupal\committee_minutes\Storage;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\committee_minutes\Entity\MinutesInterface;

/**
 * Defines the storage handler class for Minutes entities.
 *
 * This extends the base storage class, adding required special handling for
 * Minutes entities.
 *
 * @ingroup committee_minutes
 */
class MinutesStorage extends SqlContentEntityStorage implements MinutesStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(MinutesInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {committee_minutes_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {committee_minutes_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(MinutesInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {committee_minutes_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('committee_minutes_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
