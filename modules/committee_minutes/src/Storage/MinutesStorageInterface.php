<?php

namespace Drupal\committee_minutes\Storage;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\committee_minutes\Entity\MinutesInterface;

/**
 * Defines the storage handler class for Minutes entities.
 *
 * This extends the base storage class, adding required special handling for
 * Minutes entities.
 *
 * @ingroup committee_minutes
 */
interface MinutesStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Minutes revision IDs for a specific Minutes.
   *
   * @param \Drupal\committee_minutes\Entity\MinutesInterface $entity
   *   The Minutes entity.
   *
   * @return int[]
   *   Minutes revision IDs (in ascending order).
   */
  public function revisionIds(MinutesInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Minutes author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Minutes revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\committee_minutes\Entity\MinutesInterface $entity
   *   The Minutes entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(MinutesInterface $entity);

  /**
   * Unsets the language for all Minutes with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
