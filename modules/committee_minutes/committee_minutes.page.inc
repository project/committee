<?php

/**
 * @file
 * Contains committee_minutes.page.inc.
 *
 * Page callback for Minutes entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Minutes templates.
 *
 * Default template: committee_minutes.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_committee_minutes(array &$variables) {
  // Fetch Minutes Entity Object.
  $committee_minutes = $variables['elements']['#committee_minutes'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
