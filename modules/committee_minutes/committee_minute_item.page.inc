<?php

/**
 * @file
 * Contains committee_minute_item.page.inc.
 *
 * Page callback for Minute item entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Minute item templates.
 *
 * Default template: committee_minute_item.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_committee_minute_item(array &$variables) {
  // Fetch MinuteItem Entity Object.
  $committee_minute_item = $variables['elements']['#committee_minute_item'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
