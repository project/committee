<?php

namespace Drupal\committee\Form;

use Drupal\committee\Entity\CommitteeType;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CommitteeTypeForm.
 */
class CommitteeTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var CommitteeType $committee_type */
    $committee_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $committee_type->label(),
      '#description' => $this->t("Label for the Committee type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $committee_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\committee\Entity\CommitteeType::load',
      ],
      '#disabled' => !$committee_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    $form['description'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textarea',
      '#default_value' => $committee_type->getDescription(),
      '#description' => $this->t('This text will be displayed on the <em>Add committee</em> page.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $committee_type = $this->entity;
    $status = $committee_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Committee type.', [
          '%label' => $committee_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Committee type.', [
          '%label' => $committee_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($committee_type->toUrl('collection'));
  }

}
