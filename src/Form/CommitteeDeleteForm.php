<?php

namespace Drupal\committee\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Committee entities.
 *
 * @ingroup committee
 */
class CommitteeDeleteForm extends ContentEntityDeleteForm {


}
