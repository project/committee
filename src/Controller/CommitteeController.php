<?php

namespace Drupal\committee\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\committee\Entity\CommitteeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CommitteeController.
 *
 *  Returns responses for Committee routes.
 */
class CommitteeController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Committee revision.
   *
   * @param int $committee_revision
   *   The Committee revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($committee_revision) {
    $committee = $this->entityTypeManager()->getStorage('committee')
      ->loadRevision($committee_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('committee');

    return $view_builder->view($committee);
  }

  /**
   * Page title callback for a Committee revision.
   *
   * @param int $committee_revision
   *   The Committee revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($committee_revision) {
    $committee = $this->entityTypeManager()->getStorage('committee')
      ->loadRevision($committee_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $committee->label(),
      '%date' => $this->dateFormatter->format($committee->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Committee.
   *
   * @param \Drupal\committee\Entity\CommitteeInterface $committee
   *   A Committee object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(CommitteeInterface $committee) {
    $account = $this->currentUser();
    $committee_storage = $this->entityTypeManager()->getStorage('committee');

    $build['#title'] = $this->t('Revisions for %title', ['%title' => $committee->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all committee revisions") || $account->hasPermission('administer committee entities')));
    $delete_permission = (($account->hasPermission("delete all committee revisions") || $account->hasPermission('administer committee entities')));

    $rows = [];

    $vids = $committee_storage->revisionIds($committee);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\committee\CommitteeInterface $revision */
      $revision = $committee_storage->loadRevision($vid);
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $committee->getRevisionId()) {
          $link = $this->l($date, new Url('entity.committee.revision', [
            'committee' => $committee->id(),
            'committee_revision' => $vid,
          ]));
        }
        else {
          $link = $committee->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => Url::fromRoute('entity.committee.revision_revert', [
                'committee' => $committee->id(),
                'committee_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.committee.revision_delete', [
                'committee' => $committee->id(),
                'committee_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
    }

    $build['committee_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
