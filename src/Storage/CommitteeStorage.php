<?php

namespace Drupal\committee\Storage;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\committee\Entity\CommitteeInterface;

/**
 * Defines the storage handler class for Committee entities.
 *
 * This extends the base storage class, adding required special handling for
 * Committee entities.
 *
 * @ingroup committee
 */
class CommitteeStorage extends SqlContentEntityStorage implements CommitteeStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(CommitteeInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {committee_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {committee_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

}
