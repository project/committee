<?php

namespace Drupal\committee\Storage;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\committee\Entity\CommitteeInterface;

/**
 * Defines the storage handler class for Committee entities.
 *
 * This extends the base storage class, adding required special handling for
 * Committee entities.
 *
 * @ingroup committee
 */
interface CommitteeStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Committee revision IDs for a specific Committee.
   *
   * @param CommitteeInterface $entity
   *   The Committee entity.
   *
   * @return int[]
   *   Committee revision IDs (in ascending order).
   */
  public function revisionIds(CommitteeInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Committee author.
   *
   * @param AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Committee revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

}
