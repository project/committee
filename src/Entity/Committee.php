<?php

namespace Drupal\committee\Entity;

use Drupal\committee\Access\CommitteePermissionCheckerInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the Committee entity.
 *
 * @ingroup committee
 *
 * @ContentEntityType(
 *   id = "committee",
 *   label = @Translation("Committee"),
 *   label_collection = @Translation("Committees"),
 *   label_singular = @Translation("Committee"),
 *   label_plural = @Translation("Committees"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Committee",
 *     plural = "@count Committees",
 *   ),
 *   bundle_label = @Translation("Committee type"),
 *   handlers = {
 *     "storage" = "Drupal\committee\Storage\CommitteeStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\committee\CommitteeListBuilder",
 *     "views_data" = "Drupal\committee\Entity\CommitteeViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\committee\Form\CommitteeForm",
 *       "add" = "Drupal\committee\Form\CommitteeForm",
 *       "edit" = "Drupal\committee\Form\CommitteeForm",
 *       "delete" = "Drupal\committee\Form\CommitteeDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\committee\Routing\CommitteeHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\committee\Access\CommitteeAccessControlHandler",
 *   },
 *   base_table = "committee",
 *   revision_table = "committee_revision",
 *   revision_data_table = "committee_field_revision",
 *   translatable = FALSE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer committee entities",
 *   collection_permission = "view committee collection",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "owner" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "canonical" = "/committee/{committee}",
 *     "add-page" = "/committee/add",
 *     "add-form" = "/committee/add/{committee_type}",
 *     "edit-form" = "/committee/{committee}/edit",
 *     "delete-form" = "/committee/{committee}/delete",
 *     "version-history" = "/committee/{committee}/revisions",
 *     "revision" = "/committee/{committee}/revisions/{committee_revision}/view",
 *     "revision_revert" = "/committee/{committee}/revisions/{committee_revision}/revert",
 *     "revision_delete" = "/committee/{committee}/revisions/{committee_revision}/delete",
 *     "collection" = "/admin/committees",
 *   },
 *   bundle_entity_type = "committee_type",
 *   field_ui_base_route = "entity.committee_type.edit_form"
 * )
 */
class Committee extends EditorialContentEntityBase implements CommitteeInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if (($rel === 'revision_revert' || $rel === 'revision_delete') && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // If no revision author has been set explicitly,
    // make the committee owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Committee.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
