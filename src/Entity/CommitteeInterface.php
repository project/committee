<?php

namespace Drupal\committee\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Committee entities.
 *
 * @ingroup committee
 */
interface CommitteeInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Committee name.
   *
   * @return string
   *   Name of the Committee.
   */
  public function getName();

  /**
   * Sets the Committee name.
   *
   * @param string $name
   *   The Committee name.
   *
   * @return \Drupal\committee\Entity\CommitteeInterface
   *   The called Committee entity.
   */
  public function setName($name);

  /**
   * Gets the Committee creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Committee.
   */
  public function getCreatedTime();

  /**
   * Sets the Committee creation timestamp.
   *
   * @param int $timestamp
   *   The Committee creation timestamp.
   *
   * @return \Drupal\committee\Entity\CommitteeInterface
   *   The called Committee entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Committee revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Committee revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\committee\Entity\CommitteeInterface
   *   The called Committee entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Committee revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Committee revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\committee\Entity\CommitteeInterface
   *   The called Committee entity.
   */
  public function setRevisionUserId($uid);

}
