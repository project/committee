<?php

namespace Drupal\committee\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Provides an interface for defining Committee type entities.
 */
interface CommitteeTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

  // Add get/set methods for your configuration properties here.
}
