<?php

namespace Drupal\committee\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Committee type entity.
 *
 * @ConfigEntityType(
 *   id = "committee_type",
 *   label = @Translation("Committee type"),
 *   handlers = {
 *     "access" = "Drupal\entity\BundleEntityAccessControlHandler",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\committee\CommitteeTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\committee\Form\CommitteeTypeForm",
 *       "edit" = "Drupal\committee\Form\CommitteeTypeForm",
 *       "delete" = "Drupal\committee\Form\CommitteeTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\committee\Routing\CommitteeTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "committee_type",
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "description"
 *   },
 *   admin_permission = "administer committee entities",
 *   bundle_of = "committee",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/committee/settings/committee/types/{committee_type}",
 *     "add-form" = "/admin/committee/settings/committee/types/add",
 *     "edit-form" = "/admin/committee/settings/committee/types/{committee_type}/edit",
 *     "delete-form" = "/admin/committee/settings/committee/types/{committee_type}/delete",
 *     "collection" = "/admin/committee/settings/committee/types"
 *   }
 * )
 */
class CommitteeType extends ConfigEntityBundleBase implements CommitteeTypeInterface {

  /**
   * The Committee type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Committee type label.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of the group type.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
